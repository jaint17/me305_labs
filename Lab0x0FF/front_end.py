## \addtogroup Lab0xFF Lab0xFF
# @{
'''@file        front_end.py
@brief          This is the PC front end used for serial communication

@details        This front end is the primary means of how data is sent from the PC
                to the Nucleo hardware. This is done using serial communication, which 
                works by sending one byte at a time. Serial communication
                is done over the STLinkUART port that is the USB port attached
                to the Nucleo board. This way, serial data is sent directly to the Nucleo.
                When the user presses a 'g' or 'G', the data collection on the backend Nucleo begins. 
                The serial writes this character so that the Nucleo can read it and begin data collection. 
                Once 30 s of data collection is complete, the serial reads the data from the Nucleo 
                and then formats it into a CSV and plots it. 
                Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab0x0FF/front_end.py
@copyright      Copyright (C) 2021 Tim Jain - All Rights Reserved
'''

#import modules 
import serial
from matplotlib import pyplot
#numpy used for arrays
import numpy as np
#csv module used to generate csv
import csv

def getData(serial):
    '''@brief This function writes data from the serial and decodes the data received by the Nucleo
       @details This function writes the character 'g' or 'G' and encodes it in 
       ASCII (serial.write('g'.encode('ascii'))). In order for the UART to read it and decode it on the Nucleo. 
       Then, it waits for characters back from Nucleo and converts the characters to a string, decodes it and returns it
       (serial.readline().decode('ascii')), to be set up for data formatting.
       A timeout is added so it doesn't wait forever to get characters if none are sent.
       @param serial
       @return serial.readline().decode('ascii')
    '''
    timeout = 0
    serial.write('g'.encode('ascii'))
    #waiting stage until a g is pressed and sent to serial
    while serial.in_waiting == 0:
        timeout+=1
        print(timeout)
        if timeout > 1_000_000:
            print(serial.in_waiting)
            return None
        #else: 
    #reads data from Nucleo and decodes the ASCII into character 'g'
    return serial.readline().decode('ascii')
n =0
dataLoc = []
def interpretEntry(n):
    '''@brief This function interprets and formats the data list 
       @details This creates line by line entries. It converts each item of this line to float,
       creates a matrix for each line that includes the corresponding time value and function data
       value. This is called dataLoc, which will be appended to a larger formatted matrix 
       called formattedArray as discussed in the corresponding function. 
       @param n
       @return dataLoc
    '''
    #print(splitStrings)
    dataEntries = (dataList[n][0:].split(", "))
    print(dataEntries)
    global dataEntry1
    dataEntry1 = float(dataEntries[n])
    global dataEntry2
    dataEntry2 = float(dataEntries[n+1])
    dataLoc= [dataEntry1, dataEntry2]
    print(dataEntry1)
    print(dataEntry2) 
    print(dataLoc)
    return dataLoc
    n+=1

dataFormattedArray = []    
def formattedArray(dataFormattedArray):
    '''@brief This function creates a formatted data array 
       @details This formatted data array is initially an empty array which 
       uses the .extend() function to add the dataLoc array as described above upon
       each line of data sent by the Nucleo
       @param dataFormattedArray
       @return dataFormattedArray
    '''
    #this currently isn't appending what is happening in interpretEntry
    dataFormattedArray.extend(dataLoc)
    print(dataFormattedArray)
    return(dataFormattedArray)

def extraction():
    '''@brief This function extracts the time and data values of the formatted array
       @details The time array is created by slicing the first column of the formatted
       array. The data array is created by slicing the second column of the formatted array
       @param none
       @return time_points, data_values
    '''
    global time_points
    global data_values
    time_points = dataFormattedArray[0:]
    data_values = dataFormattedArray[1:]
    return time_points, data_values


def plot_figure():
    '''@brief This function plots the exponential function vs time plot
       @details The figures are generated using the matplotlib module and the
       pyplot interface, which is similar to how MATLAB plots. 

    '''
    pyplot.figure()
    pyplot.plot(time_points, data_values)
    pyplot.xlabel('Time')
    pyplot.ylabel('Data')

#testMatrix = np.matrix([[3,4], [3,5]])

def gen_CSV(dataFormattedArray):
    '''@brief This function generates a CSV out of the formatted data array
       @details The csv module is imported here. A CSV file is greated with the open("test_data.csv", "w", newline ='')
       "w" writes data to the file. newline = '' makes sure that each line of the data
       is printed on a new line. Within the CSV module, the writer by default 
       uses the comma seperated delimiter to seperate the data values. It writes 
       each line of the data as each row. 
       @param dataFormattedArray
    '''
    with open("test_data.csv","w",newline = '') as f:
        #dataString = (ser.readline().decode('ascii'))
        writer = csv.writer(f)
        writer.writerows(dataFormattedArray)
            
# The with block here will automatically close the serial port once the block 
# ends you could just make a serial object as usual instead
with serial.Serial(port='COM3',baudrate=115200,timeout=1) as ser:
    while True:
        #while getData() != None:
            user_in = input("Enter 'g' to get data from the Nucleo or 'q' to quit:")
            if user_in == 'g' or user_in == 'G':
                dataString = getData(ser)
                #print("The string is: " + dataString)
            
            # Remove line endings
                strippedString = dataString[1:-1]               

                
            # split on the commas
            # at the moment it is not putting the data values into the string
                #dataList = strippedString[1:-1].split("', ")
                dataList = strippedString[1:-1].split("', ")
                print(dataList)
                #print(splitStrings)
                interpretEntry(n)
                n+=1
                formattedArray()
                extraction()
                plot_figure()
                #print(splitStrings)
                #print("The floating point values are now: ")
                #print(float(splitStrings[0]))
                #print("and")
                #print(float(splitStrings[1]))
                gen_CSV(dataFormattedArray)
                print("sup")
                
                
            if user_in == 'q' or user_in == 'Q':
                print('Thanks')
                break
            

