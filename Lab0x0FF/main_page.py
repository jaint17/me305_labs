

'''@file main_page.py
/! \mainpage ME 305: Intro to Mechatronics
 * \section intro_sec Welcome!
 * Welcome to my portfolio! This is the introductory course in mechatronics at
Cal Poly. Within this portfolio you will see how the interaction between 
hardware and software was implemented by implementing finite state machines on 
a Nucleo microcontroller. This involved working with PWM signals, interrupts, timers,
serial-UART handshake, encoders, and motors to connect the software to hardware. 
 * \section install_sec Lab Assignments and Term Project
 * To access the lab assignments, extend the modules page on the left side 
 panel. There is a module assigned for each lab. If the images on
 Lab 0x01, Lab 0x02, or Lab 0x03 are not appearing, please extend the files
 arrow on the left side panel to view them.  
 This term project involved establishing a handshake between serial communication
and the UART on the Nucleo microcontroller. Data was sent via two COM ports to and from
the serial and the Nucleo. My focus was on this portion of the project, due to time 
constraints. A fully completed term project would display functionalities revolving around the 
encoder and motor drivers. You will also see a file that describes some of the issues with completion
involving this Term Project called hindrances.

'''
