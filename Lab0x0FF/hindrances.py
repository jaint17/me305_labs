## \addtogroup Lab0xFF Lab0xFF
# @{
'''@file hindrances.py
@brief   This describes the challenges involved in completing this term project 

 * \section new_sec Term Project Challenges
 * The purpose of this section is to describe some of the issues and hindrances
 revolving the proper completion of this term project at this time (03/18/21). 
 At the beginning of the term, I noticed that PuTTY kept crashing on my DELL laptop 
 and the window would close without an exit message or any indication why. This
 problem hindered me from completing LAB0x02 in a timely manner. First, I thought it
 was an issue with the antivirus or my internet filtering service, but disabling both 
 of those did not show any signs of luck. Thus, after multiple days of troubleshooting,
 including reading the PuTTY forums and contacting the developers, I resulted to asking
 if I could borrow a friend's laptop. Fortunately, my friend lent me his old one, 
 but it turned out to be extremely slow. It would take almost 30 minutes to boot up,
 and opening programs would take 5 minutes to load. Dealing with this was consequently very
 infuriating trying to troubleshoot programs. From there, I was able to borrow my dad's laptop,
 in which I had difficulty loading anything via ampy. However, I realized that the COM port
 settings on Device Manager had to match that of the PuTTY terminal. After troubleshooting that
 for a few hours, I was able to come to the solution. My dad's HP laptop worked smoothly 
 until a few days later, when it was severely overheating. Thus, I had to return to my 
 friend's slow laptop, which I have been working on ever since. Amidst this, I fell behind
 a few weeks. And throughout this time, I troubleshooted the serial and UART handshake
 trying to get data to send back and forth. With myself being behind the class, and 
 trying to understand this on my own, which for this Lab 1 of Lab 0x0FF I have spent 
 around 30 hours doing, I found it difficult to push through this lab to its completion,
 especially amidst the workload of other classes and final exams. I would like to make the
 appeal to re-evaluate on the project sometime during next term. 
 

'''