'''
@file fe_task_main.py
@brief The main file calling the lab 6 User Interface task from lab6_UserInt.py 
on the python kernal.
@author Tim Jain
'''

import serial
import time
from matplotlib import pyplot
import numpy as np
#from fe_motor_task_kp import FrontEndMotor_Kp
#from fe_motor_task_PI import FrontEndMotor_PI
#from fe_motor_task_PD import FrontEndMotor_PD
from fe_motor_task_PID import FrontEndMotor_PID

#task = FrontEndMotor_Kp(1200, 0.1)
#task = FrontEndMotor_PI(1200, 0.1) # initital ref velocity in RPM, time interval in seconds
#task = FrontEndMotor_PD(1200, 0.1)
task = FrontEndMotor_PID(1200, 0.1)

## Run Task
while True:
    task.run()

