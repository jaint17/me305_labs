# -*- coding: utf-8 -*-
"""
@file EncoderTask.py
@brief Interacts with the serial interface of UI and calls the objects that 
were defined in the external encoder driver class

"""
#Import relevant modules
#shares is imported so that shared variables collected there can be used here
import shares
import utime
#encoderDriver class imported here so that the positional and delta variables can 
#be used here
from encoderDriver import encoderDriverClass


class EncoderTaskClass:
        ## Initialization state 0
        S0_INIT = 0
        
        ## Update state to get encoder data
        S1_UPDATE = 1
    
        def __init__(self, interval):
            
            ## Amount of time in microseconds between runs of the task
            self.interval = int(interval)
            
            ## State to run 
            self.state = self.S0_INIT
            
            ## Counter that describes the number of times the task has run
            self.runs = 0
            
            ## Timestamp for first iteration
            self.start_time = utime.ticks_us()
            
            ## Timestamp for when the task should run next
            self.next_time = utime.ticks_add(self.start_time, self.interval)
            
        def run(self):
            '''
            @brief Runs one iteration of this task
            '''
            self.current_time = utime.ticks_us()
            ## if there is a positive time difference, run this task
            if utime.ticks_diff(self.current_time, self.next_time) >= 0:
                #If we are in the initialization state
                if (self.state == self.S0_INIT):
                    
                    #Ensures the encoder is zeroed out
                    shares.enc_zero = 0
                    
                    #Initialize encoder 1
                    #(self, ENCODER 1, timer number 3, pins B6 and B7)
                    encoderDriverClass.__init__(self, 1, 4, 'B6', 'B7')
                    #Initialize ENCODER 2 here later on
                    #encoderDriverClass.__init__(self, 2, 8, 'C6', 'C7')
                    
                    #transition to the next state which is to update the position
                    #of the encoder
                    self.transitionTo(self.S1_UPDATE)
                
                #If we are in the updating state
                elif (self.state == self.S1_UPDATE):
                    #run the update method of the encoder driver
                    encoderDriverClass.update(self)
                    
                    #import the enc_pos variable from shares and set it equal to 
                    #the get_position method integer return value from the encoder driver 
                    shares.enc_pos = encoderDriverClass.get_position(self)
                    
                    #import the enc_delta variable from shares and set it equal to 
                    #the get_delta method integer return value from the encoder driver                    
                    shares.enc_delta = encoderDriverClass.get_delta(self)
                    
                    #if the value of enc_zero variable is 1 then it resets the position
                    #of the encoder to an input and sets the enc_zero variable to 0
                    if shares.enc_zero == 1:
                        #set position with initial setting position to 0 degrees
                        encoderDriverClass.zero(self)
                        shares.enc_zero = 0
                        shares.enc_pos = 0
                        shares.enc_delta = 0
                    else:
                        pass
                else:
                    # Invalid state code (error handling)
                    pass
                
                #Increments the number of runs by 1
                self.runs +=1 
                
                # Specifies the next time that the task will run
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                    
        def transitionTo(self, newState):
            '''
            @brief Sets the new state of the FSM
            @param newState The new state number
            '''
            self.state = newState
            

    
            