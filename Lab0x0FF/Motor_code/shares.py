# -*- coding: utf-8 -*-
"""
@file shares.py

@brief This containts shared variables for EncoderTask and UITask

@details The variables here are left blank so that as EncoderTask and UITask are
running at the same time, the values can be updated by the EncoderTask and read by
UITask

@author: Tim Jain

"""

## Defines the shared position variable to be updated and read by two files
enc_pos = None

## Defines the shared delta variable to be updated and read by the two files
enc_delta = None

##Defines the shared variable which zeros the encoder position
enc_zero = None

#Shared variable, when set to 1, initiates the actuation of the motor
motor = None 

