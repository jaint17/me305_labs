## \addtogroup Lab0xFF Lab0xFF
# @{

'''@file        encoderDriver.py
@brief          This is the encoder driver class which determines the position  
                

@details        The encoder takes motion (position) and encodes it as an electrical reference. 
                We are using optical quadrature encoders to determine the position 
                of the motor. 
                Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab0x0FF/encoderDriver.py
@copyright      Copyright (C) 2021 Tim Jain - All Rights Reserved
'''
import pyb

class encoderDriverClass:
    def __init__(self, encod_num, timer_num, enc_pin1, enc_pin2):
        '''
        @brief Creates an object for the encoder and does the setup,
        given the input parameters
        @param encod_num Selects which encoder to use
        @param timer_num Selects which timer on the Nucleo to use
        @param enc_pin1 pin for set encoder
        @param enc_pin2 pin for set encoder
        '''
        # Encoder number
        self.encod_num = encod_num
        
        #Timer initialization using Time module
        self.tim = pyb.Timer(timer_num)
        
        
        #sets the prescaler (frequency divider or period multiplier) to 0
        #number of ticks that must be counted to for each timer count
        #smallest viable prescaler is 0, every tick corresponds to one timer
        #count
        #period is set to the largest 16 bit number
    
        self.tim.init(prescaler= 0, period=0xFFFF)
        self.period = 0xFFFF
        
        #defining pins for encoder class by putting them into the Pin module
        #name to use for enoder pins changes to pin_encnum
        self.pin_enc1 = pyb.Pin(enc_pin1)
        self.pin_enc2 = pyb.Pin(enc_pin2)
        
        
        #Configures the timer in encoder mode
        self.tim.channel(1, pin = self.pin_enc1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin = self.pin_enc2, mode = pyb.Timer.ENC_AB)
        
        #Initialize positions of encoder 
        self.current_position = 0
        self.previous_position = 0
        self.offset = 0
        
    def update(self):
          '''
          @brief Updates the recorded position of the encoder when called regularly
          '''
          #updates the previous position of the encoder to the current position
          #of the encoder
          self.previous_positon = self.current_position
          
          #sets the current position of the encoder to the counter() method of the timer
          #which is the timer's counter value and subtracts the offset which will be
          #shown later
          #the offset is basically just the current position as defined in 
          #set_position method
          self.current_position = self.tim.counter() - self.offset

          #delta is the reading value. It takes the current value of the encoder
          #and subtracts it from the previous position of the encoder
          self.delta = self.current_position - self.previous_position 
          
          
    def get_delta(self):
        '''
        @brief Takes the delta value (reading) of the encoder and returns the difference in 
        recorded position between the two most recent calls to update(). It ensures that 
        good delta is given, and that bad deltas are filtered out if there is overflow 
        past the period
        '''
        #corrects bad delta case here:
        #if the delta value is greater than half of the period, it corrects it
        #by taking the (delta = current position - previous position) 
        # and subtracting the period from that
        
        if self.delta > (self.period/2):
            self.good_delta = (self.current_position - self.previous_position) - self.period
        
        # #corrects bad delta case here:
        # #if the delta value is less than the negative of half of the period, 
        # # it corrects it
        # #by taking the (delta = current position - previous position) 
        # # and adding the period from that    
        
        elif self.delta < (-self.period/2):
            self.good_delta = (self.current_position - self.previous_position) - self.period
        
        # # if delta is 0, then the good delta will just be 0
        
        elif self.delta == 0:
            self.good_delta = 0
        
        # #in other cases, the good delta is = to the delta value then the good delta is just
        # #equal to the delta value
        
        else:
            self.good_delta = self.delta
        
        # #returns the integer of this good delta value to be used in our data
        # #collection
        
        return int(self.good_delta)
    
    
        # if self.delta < (-0xFFFF/2):
        #     self.good_delta = (0XFFFF-self.previous_position) +self.current_position
        # elif self.delta > (0xFFFF/2):
        #     self.good_delta = -((0xFFFF-self.current_position)+self.previous_position)
        # elif self.delta == 0:
        #     self.good_delta = 0
        # else:
        #     self.good_delta = self.delta
        # return int(self.good_delta)
    
        
    def get_position(self):
        ''' 
        @brief Returns the enoder's current position
        '''
        return int(self.current_position)
    
    def set_position(self, position):
        '''
        @brief Resets the position to a specified value that is input
        '''
        #sets the offset to set the position in the update() method for getting the updated
        #recorded position of the encoder
        #position is input by the user
        self.offset = self.current_position
        self.current_position = position
        return int(self.current_position)
    
    def zero(self):
        '''
        @brief Zeros out the encoder
        '''
        self.offset = self.current_position
        self.current_position = 0
        return int(self.current_position)
    
    
 
            
            
            
            
