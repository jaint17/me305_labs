'''
@file Closed_LoopPID.py
@brief Script defining a class for a Closed Loop PID-controller.  
@details When called regularly, 
this computes and returns the actuation value based on the measured and reference 
values. The class can also return the current Kp value as well as set the Kp value.
@author Tim Jain
'''

class ClosedLoopPID:

    def __init__(self, Kp, Ki, Kd, min_val, max_val):
        '''
        @brief Creates a Closed Loop object, initites feedback system
        @param Kp predefined proportionality constant 
        @param min_val lower saturation limit for the system
        @param max_val upper saturation limit for the system
        '''
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.max_val = max_val
        self.min_val = min_val
        
    def update(self, ref_pos, ref_vel, meas_pos, meas_vel, meas_acc):
        '''
        @brief Given a single input reference value and a measured value, calculates actuation value 
        (PWM level), which is the ratio of Vm to Vdc, by using the predefined Kp 
        within the predefined saturation limits. It ensures that the motor 
        does not over saturate past its limits
        @param ref_val input reference (desired) velocity value
        @param meas_val input actual (measured) velocity value
        
        '''
        #ref_val is the reference (desired) velocity of the output shaft
        #meas__val is the measured velocity of the output shaft after being 
        #controlled with proportional control, motor control
        L = self.Kp*(ref_vel - meas_vel) + self.Ki*(ref_pos-meas_pos) + self.Kd*(-meas_acc)
        if L < self.min_val:
            L = self.min_val
        elif L > self.max_val:
            L = self.max_val
        return L
        
    def get_Kp(self):
        '''
        @brief Gets the gain Kp value and returns it
        '''
        return self.Kp

    def get_Ki(self):
         '''
        @brief Gets the gain Ki value and returns it
        '''
         return self.Ki
     
    def get_Kd(self):
         '''
        @brief Gets the gain Kd value and returns it
        '''
         return self.Kd
       
    def set_Kp(self, Kp_new):
        '''
        @brief Sets the gain Kp value to the new value specified by the user
        @param new_val new input value for proportionality constant
        '''
        self.Kp = Kp_new

    def set_Ki(self, Ki_new):
        '''
        @brief Sets the gain Ki value to the new value specified by the user
        @param new_val new input value for proportionality constant
        '''
        self.Ki = Ki_new
     
    def set_Kd(self, Kd_new):
        '''
        @brief Sets the gain Kd value to the new value specified by the user
        @param new_val new input value for proportionality constant
        '''
        self.Kd = Kd_new

