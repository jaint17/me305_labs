# -*- coding: utf-8 -*-
"""
@file main.py

@brief This file is the main file for the encoder task and UI task

@details Defines both of the EncoderTask and UITask and run them with an interval
of one second. It should run for 60 s

"""
import pyb
from pyb import UART as UART
import utime
from encoderDriver import encoderDriverClass
from motorDriver import MotorDriver
#from MotorTask_Kp import MotorTaskClass_Kp
#from MotorTask_PI import MotorTaskClass_PI
#from MotorTask_PD import MotorTaskClass_PD
from MotorTask_PID import MotorTaskClass_PID

#from Closed_LoopKp import ClosedLoopKp
#from Closed_LoopPI import ClosedLoopPI
#from Closed_LoopPD import ClosedLoopPD
from Closed_LoopPID import ClosedLoopPID

# interval is 20 ms (20000 microseconds)
#Task1 = MotorTaskClass_Kp(10000)
#Task1 = MotorTaskClass_PI(10000)
#Task1 = MotorTaskClass_PD(10000)
Task1 = MotorTaskClass_PID(10000)

#While loop 
while True:
    Task1.run()

    #There seems to be some interference within tasks. Probably best to 
    #set up a seperate main for the motor 
    #Task3.run()
    
    
    