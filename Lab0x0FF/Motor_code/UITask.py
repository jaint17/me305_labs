# -*- coding: utf-8 -*-
"""
@file UITask.py

@brief This task handles the user interface which comes from user inputs of 
a list of different character inputs

@details Implements a finite state machine that takes commands which
cause the Nucleo to perform a task
z: zero the encoder 1 position
p: print out the encoder 1 position
d: print out the encoder 1 delta
g: collect encoder data 1 data for 30 s and generate plot
s: end data collection prematurely

"""

import shares
from pyb import UART
import utime
from math import *
class UITaskClass: 
    
    ## Defining initial state 0
    S0_INIT = 0
    
    ## Interface handling state
    S1_INTERFACE = 1
    
    ##Another Command?
    S2_WAIT_STAGE = 2

    def __init__(self, interval):
        ''' 
        @brief Creates the UI Task object
        @param interval input for how long the time between runs is in milliseconds
        '''
        ## Amount of time in milliseconds between runs of the task
        self.interval = int(interval)
        
        ## State to run 
        self.state = self.S0_INIT
        
        ##Runs for counter on how many times the task has run
        self.runs = 0 
        
        ## Timestamp for first iteration
        self.start_time = utime.ticks_us()
        
        ## Timestamp for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ##serial port
        self.myuart = UART(2)
        
        self.array_data = []
        self.array_time = []
        self.array_chars = []
        self.array_values = []
        
        
    def run(self): 
        '''
        @brief runs one iteration of the task
        '''
        self.current_time = utime.ticks_us()
        if utime.ticks_diff(self.current_time, self.next_time) >= 0:
            if (self.state == self.S0_INIT):
                                
                print('''
                  Please type a command, 
                  Type z to zero the encoder position
                  Type p to print out the encoder position
                  Type d to print out the encoder delta
                  Type g to collect encoder data for 30 s and generate plot
                  Type s to end data collection prematurely
                  Type m to start the motoring process
                  ''')
                 
                 #transition to the next state, which is the interface state
                self.transitionTo(self.S1_INTERFACE)
            
            #if we are in the interface state
            elif (self.state == self.S1_INTERFACE):
                
                #if there is any reading from UART,
                if self.myuart.any():
                    #read the character that was sent via Serial
                    self.char = self.myuart.readchar()
                    
                    #if the character is a 'z' (ASCII 122), set the enc_zero 
                    #variable equal to 1, and that value of the variable will be set to 
                    #0 
                    #Zeros out the enoder
                    if self.char == 122:
                        shares.enc_zero = 1
                        print("The encoder has been zeroed: " + str(shares.enc_pos) + "Degrees")
                        #shares.enc_zero = 1
                    
                    #If the character is a 'p' (ASCII 112), print out the position
                    #of the encoder. shares.enc_pos was updated in EncoderTask
                    elif self.char == 112:
                        print("The current encoder position is: " + str(shares.enc_pos) + "Degrees")

                    #If the character is a 'd' (ASCII 100), print out the delta
                    #of the encoder. shares.enc_delta was updated in EncoderTask                    
                    elif self.char == 100:
                        print("Delta in position of encoder is: " + str(shares.enc_delta) + "Degrees")
                        
                    elif self.char == 103:
                        print(str(shares.enc_pos))
                        self.myuart.write(str(shares.enc_pos).encode('ascii'))
                    
                    elif self.char == 115:
                        print("Thanks for collecting data with us!")
                    
                    elif self.char == 109:
                        shares.motor = 1
                        #print("Motoring")
                    else:
                        print("Invalid input: Try again")
                        #self.transitionTo(self.S2_WAIT_STAGE)
               
                else:
                    pass
                
            elif (self.state == self.S2_WAIT_STAGE):
                print(''' Another command?
                  Type z to zero the encoder position
                  Type p to print out the encoder position
                  Type d to print out the encoder delta command?
                  ''')
                self.transitionTo(self.S1_INTERFACE)
                
            else: 
                pass
        else:
            pass
        
        # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)                
                    
    def transitionTo(self, newState):
        '''
        @brief Sets the new state of the FSM
        @param newState The new state number
        '''
        self.state = newState

                        # self.startTime = utime.ticks_us() #assign the current time to self.startTime
                        # self.time = self.startTime-self.startTime #set the time to 0 
                        # n = 0
                        # self.array_time.append(self.time/1000000)   #append this initial value to the time array
                        # self.val = shares.enc_pos #compute the initial value with initial time
                        # self.array_values.append(self.val)
                        # print("Initial Values")
                        # print(self.time)
                        # print(self.val)
                        # print("=====")
                        
                        # #This won't update the encoder position because it's running within 1 task!!
                        
                        
                        # while self.time <= (30200000): #set to 30200000 to be 30.2 s, because we want to see it capture the point at 30 s
                        #     if (self.time - self.interval*n)>=self.interval:   
                        #         self.array_time.append(self.time/1000000) #append the value of self.time and convert from ms to s
                        #         self.val = shares.enc_pos 
                        #         self.array_values.append(self.val) 
                        #         print(self.time)
                        #         print(self.val)
                        #         print("n = " + str(n))
                        #         n += 1
                        #     self.time = utime.ticks_us()-self.startTime #reassign the value of self.time by getting the updated time 
                        #     #and subtracting the initial counter time
                        # print(self.array_time)
                        # print(self.array_values)
                        # #write time array and values array as strings to UART
                        # self.myuart.write(str(self.array_time).encode('ascii'))
                        # self.myuart.write(str(':').encode('ascii'))
                        # self.myuart.write(str(self.array_values).encode('ascii'))
                        # print("data collection complete")                    