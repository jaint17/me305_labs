# -*- coding: utf-8 -*-
"""
Created on Sun May 30 20:33:21 2021

@author: jaint
"""

''' @file main.py
There must be a docstring at the beginning of a Python
source file with an @file [filename] tag in it! '''

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''

    def __init__ (self, motorNum, nSLEEP_pin, IN1_pin, IN2_pin, tim):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. '''
        #nSLEEP pin is assigned the Pin module and then the OUT_PP method
        #which configures the pin for output, with push-pull control
        self.nSLEEP_pin = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        self.motorNum = motorNum
        
        
        #timer triggers at 20000 Hz

        #timer is configured to channels 1 and 2 in PWM mode given the motor pins
        if (self.motorNum == '1'): 
            #Defines motor Pins for negative and positive terminals of M1 respectively
            self.M1_NEG = pyb.Pin(IN1_pin)
            self.M1_POS = pyb.Pin(IN2_pin)
            
            #sets timer for M1, uses channels 1 and 2
            self.tim1 = pyb.Timer(tim, freq = 20000)  
            
            self.chan1 = self.tim1.channel(1, pyb.Timer.PWM, pin = self.M1_NEG)
            self.chan2 = self.tim1.channel(2, pyb.Timer.PWM, pin = self.M1_POS)
            
            self.chan1.pulse_width_percent(0)
            self.chan2.pulse_width_percent(0)
            
            self.nSLEEP_pin.low()
            
        if (self.motorNum == '2'): 
            #Defines motor Pins for negative and positive terminals of M1 respectively
            
            self.M2_NEG = pyb.Pin(IN1_pin)
            self.M2_POS = pyb.Pin(IN2_pin)

            #sets timer for M2, uses channels 3 and 4
            self.tim2 = pyb.Timer(tim, freq = 20000)     
            
            self.chan3 = self.tim2.channel(3, pyb.Timer.PWM, pin = self.M2_NEG)
            self.chan4 = self.tim2.channel(4, pyb.Timer.PWM, pin = self.M2_POS)   
            
            self.chan3.pulse_width_percent(0)
            self.chan4.pulse_width_percent(0)
            
            self.nSLEEP_pin.low()
        #Set PWM of channels to 0 to initialize
        

        
        #Start by setting this pin to low by default so that the motor is not 
        #actuated upon start initialization

        print ('Creating a motor driver again')

    def enable (self):
        '''
        @brief Sets pin_nSLEEP to high, allowing motor to be actuated
        '''
        self.nSLEEP_pin.high()
        print ('Enabling Motor')

    def disable (self):
        '''
        @brief Sets pin_nSLEEP to low, disabling the motor from being actuated
        '''
        self.nSLEEP_pin.low()
        #print ('Disabling Motor')

    def set_duty (self, duty):
        ''' 
        @brief This method sets the duty cycle to be sent
        to the motor to the given level. 
        @details Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor 
        '''
        #Forward direction
        if (self.motorNum == '1'):
            if duty >0:
                self.chan1.pulse_width_percent(duty)
                self.chan2.pulse_width_percent(0)
        
            elif duty <0:
            #absolute value since a positive duty must go into the PWM signal
                duty = abs(duty)
                self.chan1.pulse_width_percent(0)
                self.chan2.pulse_width_percent(duty)
                
            else: 
                self.chan1.pulse_width_percent(0)
                self.chan2.pulse_width_percent(0)
        
        elif (self.motorNum == '2'):

            
            if duty >0:
                self.chan3.pulse_width_percent(duty)
                self.chan4.pulse_width_percent(0)
        
            elif duty <0:
            #absolute value since a positive duty must go into the PWM signal
                duty = abs(duty)
                self.chan3.pulse_width_percent(0)
                self.chan4.pulse_width_percent(duty)   

            else: 
                self.chan3.pulse_width_percent(0)
                self.chan4.pulse_width_percent(0)
                
if __name__ == '__main__':

    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    #The motor we are using
    #motorNum = 1
    #motorNum = 2
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = 'A15';
    print('Trial 4')
    moto_in = input('Enter the motor you want to use: ')
    motorNum = int(moto_in)
    
    if moto_in == '1':
        
        #FOR MOTOR 1
        pin_IN1 = 'B4';
        pin_IN2 = 'B5';
    
    elif moto_in == '2':
        #FOR MOTOR 2
        pin_IN1 = 'B0'
        pin_IN2 = 'B1'
    
    # Create the timer object used for PWM generation
    #We always will use timer 3
    tim = 3;
    
    # Create a motor object passing in the pins and timer
    moto = MotorDriver(motorNum, pin_nSLEEP, pin_IN1, pin_IN2, tim)
    moto.set_duty(0)
    moto.disable()    
    # Enable the motor driver
    moto.enable()
    
    #User Interface
    #User has the opportunity to choose the duty cycle multiple times, then the motor
    #powers off
    
    n = 1
    while n <= 5:
        user_duty = input('Please input your motor speed for this motor! From (-100 to 100): ')
        moto.set_duty(int(user_duty))
        n+=1
    
    moto.set_duty(0)
    moto.disable()