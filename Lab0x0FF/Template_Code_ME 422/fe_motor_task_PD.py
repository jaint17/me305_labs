'''
@file fe_motor_task.py
@brief User interface task as a finite state machine. 
@details This user interface controls 
a closed-loop p-controller on an Nucleo L476 devboard that controls the speed of 
a DC motor using a step input reference value of 1200 RPM. It sends a reference 
speed and a Kp Gain value to the Nucleo, then plots the resulting motor speed response.
@author Tim Jain
'''

import time
import serial
import matplotlib.pyplot as plt
import sys
# initialize serial port for PC to Nucleo communication

ser = serial.Serial(port='COM5',baudrate=115200,timeout=1)


class FrontEndMotor_PD:
    '''
    @brief User interface task for ME 305 lab 6 that sends Kp and reference velocity inputs to the Nucleo, then receives and plots the Motor Velocity Response data
    '''

    ## Initialization state: setting Kp
    S0_INIT             = 0
    
    ## Collecting and plotting data
    S1_DATA             = 1
    
    ## Finishes FSM loop and exits task; note that this task only runs once
    S2_FINISH           = 2
    
    def __init__(self, ref_velocity, interval):
        '''
        @brief Creates a user interface task object.
        @param ref_velocity defines the desired reference at which the motor should spin 
        @param interval defines the interval at which the task will loop.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## stores a class copy of the serial initialization
        self.ser = ser
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
        ## the input reference motor speed [RPM]
        self.ref_vel = ref_velocity
        

    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''  
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                    # Run State 0 Code
                Kp_init = input('Specify a Kp input please. Kp = ')
                self.Kp = int(float(Kp_init)*1E6)/1E6
                if self.Kp <= 0:
                    In_KP = input('Please input a positive, nonzero value. Kp = ')
                    self.Kp = int(float(In_KP)*1E6)/1E6
                elif self.Kp > 0:
                    Kp = str(self.Kp)+'\n'
                    ref_vel = str(self.ref_vel)

                Kd_init = input('Specify a Kd input please. Kd = ')
                self.Kd = int(float(Kd_init)*1E6)/1E6                
                if self.Kd <= 0:
                    In_KD = input('Please input a positive, nonzero value. Kd = ')
                    self.Kd = int(float(In_KD)*1E6)/1E6
                elif self.Kd > 0:
                    Kd = str(self.Kd)+'\n'
                    ref_vel = str(self.ref_vel)
                    
                    
                    # writes input controller parameters to initialize the program
                    #sends two lines of serial data, one for Kp, the other for the reference data
                    self.ser.write(Kp.encode())
                    self.ser.write(Kd.encode())
                    self.ser.write(ref_vel.encode())
                    
                    print('''
                          Hello there! General Kenobi. We are interfacing with a Closed Loop 
PI-controller to control the speed of the motor using your 
Nucleo L476 devboard. Running the controller with Kp = {}, Kd = {} and motor reference velocity of {} RPM. 
                          '''.format(self.Kp, self.Kd, self.ref_vel))
                    
                    self.transitionTo(self.S1_DATA)
                
            elif(self.state == self.S1_DATA):
                self.term = input('To stop data collection, press s: ')
                if self.term == 's':
                    #Send input
                    self.ser.write(str(self.term).encode('ascii'))
                    #Clear Command
                    self.term = None
                        
                    #Initiate data reading
                    #reads the array with the time, motor velocity, and reference velocity 
                    data = list(ser.readline().decode('ascii').split('  '))
                    out = [m.strip('[]').split(', ') for m in data]
                    time_stamp = [float(t) for t in out[0]]
                    motor_vel = [float(t) for t in out[1]]
                    input_vel = [float(t) for t in out[2]]
                    
                    #the self.time array is based off of when the Nucleo is 
                    #first initialized, rather than when the motor starts 
                    #spinning. Thus, must subtract each element by timestamp[0] to 
                    #set t=0 to when the motor starts spinning
                    rel_time = [t - time_stamp[0] for t in time_stamp]
                    
                    #Plotting Data
                    plt.plot(rel_time, motor_vel, 'b', rel_time, input_vel, 'r--')
                    plt.xlabel('Time (s)')
                    plt.ylabel('Motor Velocity (RPM)')
                    plt.title('Motor Velocity Response compared to Input Reference Velocity with Kp = '+str(self.Kp) + ' and Kd = ' +str(self.Kd))
                    
                    self.transitionTo(self.S2_FINISH)
                    
                elif self.term != 's':
                    self.term = input('Please enter a valid command (s): ')
            
            elif(self.state == self.S2_FINISH):
                sys.exit()
                        
            self.next_time += self.interval

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState
