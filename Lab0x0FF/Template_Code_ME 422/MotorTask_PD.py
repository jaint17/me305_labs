'''
@file .py
@brief Task that actuates motor and records response data on the Nucleo. 
@details This file operates on the Nucleo L476 and interacts with lab6_UserInt.py 
on the PC to receive user input Kp and Reference Velocity values, actuate the 
motor according to these inputs, record and format control data, and send the 
relevant, formatted data back to lab6_UserInt.py.
@author Tim Jain
'''

import pyb
from pyb import UART as UART
import utime
import shares
from encoderDriver import encoderDriverClass
from Closed_LoopPD import ClosedLoopPD
from motorDriver import MotorDriver
import sys
from math import *

class MotorTaskClass_PD:
    '''
    @brief Motor task that receives user inputs from the PC, actuates 
    the motor, records motor speed data, and sends this data back to the PC
    '''
    ## Initializing 
    S0_INIT  = 0
    
    ## Controlling Data Collection
    S1_CONTROLLING = 1
    
    ## Ends FSM loop and exits the task; note that this task only runs once 
    S2_FINISH = 2
    
    def __init__(self, interval):
        '''
        @brief Creates a motor task object
        @param interval defines the interval at which the task will loop.
        '''

        ## Initiate Serial Port Communication
        self.myuart = UART(2)
        
        ## stores copies of encoder, motordriver, and closedloop classes
        #Encoder is encoder number 1, timer 4, pins B6 and B7 corresponding to 
        #encoder 1
        #self.Encoder = encoderDriverClass(1, 4, 'B6', 'B7') 
        self.Encoder = encoderDriverClass(2, 8, 'C6', 'C7')
        #Motor is motor number 1, sleep pin A15, pins B4 and B5 corresponding to 
        #motor 1, timer number 3
        #self.MotorDriver = MotorDriver('1', 'A15', 'B4', 'B5', 3)
        self.MotorDriver = MotorDriver('2', 'A15', 'B0', 'B1', 3)
        self.closedloop = ClosedLoopPI
    
        ## updates initial encoder position
        self.Encoder.update()
        
        ## The amount of time in microseconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Enables motor to spin
        self.MotorDriver.enable()
        self.MotorDriver.set_duty(0)
        
        ## Preallocate Data Arrays
        self.time = []
        self.mot_velocity = []
        self.ref_velocity = []
        
        ## Initial State
        self.state = self.S0_INIT
        
    def run(self):
        '''
        @brief Runs one iteration of the task.
        '''
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            self.Encoder.update()
            if (self.state == self.S0_INIT):
                    #self.char = self.myuart.readchar()
                    if self.myuart.any():  #it reads the Kp from the front end
                    ## Read Serial input and assign reference velocity
                    # Serial lines for Kp and ref_vel, respectively are being sent from the front end
                        self.Kp = float(self.myuart.readline().decode('ascii'))
                        self.Kd = float(self.myuart.readline().decode('ascii'))
                        self.ref_vel = int(self.myuart.readline().decode('ascii'))
    
                    ## Initiate closedloop controller class with the Kp value received from the PC and PWM saturation limits of -100 and 100
                        self.ClosedLoopPD = self.closedloop(self.Kp, self.Kd, -100, 100)
                        self.transitionTo(self.S1_CONTROLLING)
                    
            if (self.state == self.S1_CONTROLLING):
                #if nothing is being sent by serial
                if self.myuart.any() == 0:
                    ## Read Encoder Value and Convert to RPM
                    PPR = 4000
                    revs = self.Encoder.get_delta()/PPR #converts encoder position delta to number of revolutions in motor shaft
                    int_sec = self.interval*1E-6 #converts interval value from microseconds to seconds
                    int_min = int_sec/60 #converts interval value to minutes
                    self.ref_pos = self.ref_vel*int_min
                    actual_velocity = revs/int_min # calculates motor speed in RPM, this is the actual velocity, based on the encoder delta
                    actual_acceleration = (actual_velocity)/int_min
                    
                    ## Write relevant data to preallocated arrays
                    self.mot_velocity.append(actual_velocity)
                    self.ref_velocity.append(self.ref_vel)
                    self.time.append((self.curr_time-self.start_time)/1E6) #Converts from microseconds to seconds
                    
                    ## Calculate Control Loop Gain: Actuation Signal 
                    L = self.ClosedLoopPD.update(self.ref_vel, actual_velocity, actual_acceleration)
                    actual_velocity = None
                    actual_acceleration = None
                    ## Change motor duty cycle based on Control Loop Output, 
                    self.MotorDriver.set_duty(L)
                    
                else:
                    #print("Duty should be set to 0 now")
                    self.MotorDriver.set_duty(0)
                    self.MotorDriver.disable()
                    #time, motor velocity, reference velocity
                    #reference velocity stays constant 
                    self.myuart.write(str(self.time)+'  '+str(self.mot_velocity)+'  '+str(self.ref_velocity))
                    self.transitionTo(self.S2_FINISH)
                    
            if (self.state == self.S2_FINISH):
                sys.exit()
                #print("We made it")
                pass
                    
            else:
                pass
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                    
    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        @param newState The new state variable for the FSM
        '''
        self.state = newState
            
        