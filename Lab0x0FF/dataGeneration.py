## \addtogroup Lab0xFF Lab0xFF
# @{

'''@file        dataGeneration.py
@brief          Upon receiving the character 'g' or 'G', this class generate
                data for 30 s and sends it to the front end to be formatted. 
                
@details        The class implements a finite state machine to receive the serial 
                data by the UART, which is the hardware
                device for asynchronous serial communication. The UART reads
                the data sent from the Serial and decodes it from ASCII back to 
                character form. The data is taken for 30 seconds, and data points
                are taken with timing defined by a period, which is set in the main
                backend script. The incremented input value is what the exponentially 
                decaying sinewave takes in to compute the value of the function. 
                As this is done, an array is created and the corresponding time 
                and function values are appended to those. Once this is done, 
                the data array is printed and then sent to serial front end 
                to be formatted. 
                Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab0x0FF/dataGeneration.py
               
@image          html LAB0X0FF_CLASS_FSM_PART1.jpg width=1000px              
@copyright      Copyright (C) 2021 Tim Jain - All Rights Reserved
'''

import pyb
from pyb import UART
import utime
from math import exp, sin, pi

pyb.repl_uart(None)
# Check for characters, if one is received respond to the PC with a formatted
# string
class dataGeneration:
        '''@brief This class generates data on the Nucleo when the character 'g' 
        or'G' is received from the serial. 
        @details Described above. 
        '''
    
        def __init__(self, taskNum, period):
                '''@brief This function initializes the class with the appropriate attributes
                @param self, taskNum, period
                '''
                self.S0_INIT = 0
                self.S1_DATA_COLLECTION = 1
                self.S2_PRINT_CONSOLE = 2
                self.S3_END_STATE = 3
                self.state = 0
                self.runs = 0
                self.period = period
                self.startTime = utime.ticks_ms()
                self.nextTime = utime.ticks_add(self.startTime, self.period)
                self.thisTime = utime.ticks_ms()
                self.chars_received = 0
                self.taskNum = taskNum 
                self.myuart = UART(2)
                self.array_data = []
                self.array_time = []
                
    
        def transitionTo(self, newState):
            '''@brief This function transitions the state to the next state
            @details The state is reassigned to the new state. 
            @param self, newState
            '''
            print(str(self.state) + "->" + str(newState))
            self.state = newState
            
        def rundata(self):
            '''@brief This functions runs the FSM. It is used as a task in 
            main.py
            @details The zeroth state of the FSM is the initialization state. 
            It immediately transitions to the next state to start begin 
            data collection. The first state is implemented if it receives a signal 
            from serial. If so, it reads it and decodes it into the corresponding character
            'g' or 'G'. The utime module is used in order to control the timing of 
            the 30 s for which the data collection is performed. A startTime is defined
            and then a nextTime is defined. nextTime is defined by utilizing a period
            which defines the spacing or delay in which the time values are incremented. 
            This is done using the utime.ticks_add() function.
            This controls the how many "chars_received " are used to compute the value
            "val" of the exponentially decaying sine function since the variable in computing
            this is dependent on the "chars_received" which increments per iteration of the 
            while loop, which is defined to collect data while time is less than 30 s, represented
            by the utime.ticks_diff() function. 
            An array of these these chars_received and val is created. Upon each iteration
            of the loop, the corresponding values are appended to this data array. 
            Once this process is complete, we transition to the data printing state, which
            prints the corresponding data array created by the loop and encodes it 
            over UART to be sent to serial to be read and formatted into a CSV. 
            @param self
            '''
            #print("test 23") 
            if self.state==self.S0_INIT:
                self.transitionTo(self.S1_DATA_COLLECTION)
                print("Begin data generation now ")
                
            elif self.state==self.S1_DATA_COLLECTION:
                if self.myuart.any():
                    self.in_char = self.myuart.read().decode('ascii')
                    if (self.in_char == 'g' or self.in_char == 'G'):
                    #while utime.ticks_diff(self.nextTime, self.thisTime) < 29000:
                        self.startTime = utime.ticks_ms()
                        self.nextTime = utime.ticks_add(self.startTime, self.period)
                        self.thisTime = utime.ticks_ms()
                        while (utime.ticks_diff(self.nextTime, self.thisTime) < 30000):
                            self.nextTime = utime.ticks_add(self.nextTime, self.period) 
                            self.runs += 1
                            self.array_time.append((utime.ticks_diff(self.nextTime, self.thisTime)))
                            self.chars_received += 1.0
                            self.val = exp(-self.chars_received/10)*sin(2*pi/3*self.chars_received)
                            self.out_string = '{:}, {:}'.format(self.chars_received, self.val)
                            self.array_data.append((self.out_string))
                            print(self.runs)
                        self.transitionTo(self.S2_PRINT_CONSOLE)
                        print("Commence data printing stage")

            elif self.state==self.S2_PRINT_CONSOLE:   
                self.transitionTo(self.S3_END_STATE)
                print("Data will now be printed to the console")
                #print(self.array_time)
                print(self.array_data)
                #print(self.out_string)
                self.myuart.write(str(self.array_data).encode('ascii'))
                #self.myuart.write(str(self.array_data)).encode('ascii')
                print("end program")
                
            else:
                pass
