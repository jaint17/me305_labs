## \addtogroup Lab0xFF Lab0xFF
# @{

'''@file        encoderDriver.py
@brief          This is the encoder driver which determines the position  
                

@details        The encoder takes motion (position) and encodes it as an electrical reference. 
                We are using optical quadrature encoders to determine the position 
                of the motor. 
                Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab0x0FF/encoderDriver.py
@copyright      Copyright (C) 2021 Tim Jain - All Rights Reserved
'''

# class encoderDriver:
#     def __init__(self, encod_num, timer_num, ch1_pin, ch2_pin):
#         if self.encod_num == 1:
            
