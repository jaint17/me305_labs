## \addtogroup Lab0xFF Lab0xFF
# @{

'''@file        main.py
@brief          This is the backend main script
@details        This is the Nucleo backend script that runs the dataGeneration class. It runs it as
                a task with a set period to control the timing and how many data
                points are printed to the array.
                Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab0x0FF/main.py
@copyright      Copyright (C) 2021 Tim Jain - All Rights Reserved
'''

import pyb
from pyb import UART
import utime
#from array import array
#from matplotlib import pyplot
from math import exp, sin, pi


pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

pyb.repl_uart(None)
S0_INIT = 0
S1_DATAGENERATION = 1
S2_DATAFORMAT = 2

print("newchanges")

#This was here in case the FSM was going to be implemented on the backend, which
#it is not because it is implemented on the dataGeneration class. 
def transitionTo(newState):
    global state
    print(str(state) + "->" + str(newState))
    state+=1
    
# Check for characters, if one is received respond to the PC with a formatted
# string
from dataGeneration import dataGeneration           
# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
state = 0
if __name__ == "__main__":
    # Program initialization goes here
    task1 = dataGeneration(1, 500)
    #task2 = dataGeneration(2, 500)
    
    while True:
        try:
            #if state == S0_INIT:
                #transitionTo(S1_DATAGENERATION)
            
            #elif state == S1_DATAGENERATION:
                task1.rundata()
                #serial.write
                #transitionTo(S2_DATAFORMAT)
            
            #if state == S2_DATAFORMAT:
                #print("end")
                #task2.rundata()
                #break
        
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break
                                
                    #if utime.ticks_diff(self.nextTime, self.startTime) > 3000:
                       # self.transitionTo(self.S2_PRINT_CONSOLE)
                        
        # Read one character and turn it into a string
        
        # If the character was a 'g' respond with a formatted string encoded as
    