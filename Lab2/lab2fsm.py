## \addtogroup Lab0x02 Lab0x02
# @{
'''@file        lab2fsm.py
@brief          Cycles through a set of wave patterns through LEDs
@details        Implements a finite state machine, shown below, based on the user pressing the blue
                user button on the Nucleo. The FSM runs based on the external interrupt, changing states
                for each wave pattern when the button is pressed.
@image          html lab2_FSM.jpg 
                Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab2/lab2fsm.py
                
                Video: https://www.youtube.com/watch?v=e1LUuo6ac8M
@author         Tim Jain
@date           02/02/21
'''
# put image in the html folder of lab staging area and it will appear

import utime
import math
import pyb


state = 0

def onButtonPressFCN(IRQ_src):
    '''@brief  Callback function for external interrupt
       @details When the blue user button is pressed on the Nucleo, this callback function is referred to, which changes the state, and
       the wave pattern of the LED.
       @param IRQ_src is the parameter within the callback function that detects whether a falling edge
       is detected.
       '''
    global state
    state += 1
    return state
    print('Button pressed')
    

# The pins for the LED and the blue user button are defined by using the Pin class.    
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)   
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
# The external interrupt for the blue user button is defined here, referring to the callback function
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
#initialization variable for sine wave position
x = 0

# utime.ticks_ms() is a timestamp counter in milliseconds. startTime and nextTime gives us two times to work with
startTime = utime.ticks_ms()
nextTime = utime.ticks_ms()
# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    print("Welcome! Prepare to be amazed...Right now you can see a sine pattern but press the blue user button and see what happens :) !")
    while True:
        #generally the Timer class is used to create a channel to generate a PWM signal on the LEDs
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        try:        
            if state % 3 ==0:
                print('Square wave selected')
                while state % 3 == 0:
                    #rising edge
                    #measures the difference between the two time stamps and takes the modulus, which means the value 
                    #will not exceed 1000. If it is less than 500 ms and the state is in state0
                    #as defined above in the callback function, the LED will be on
                    #the modulus is used here again so that the maximum state
                    #cannot exceed 2
                    while utime.ticks_diff(nextTime, startTime) % 1000 <= 500 and state % 3 ==0:
                        #redefines nextTime to increment it
                        nextTime = utime.ticks_ms()
                        #LED is on 100%
                        t2ch1.pulse_width_percent(100)        
                    #falling edge 
                    #if the time difference is greater than 500 the LED will be off,
                    #but eventually the modulus will reset at 0 and the LED will be on again
                    while utime.ticks_diff(nextTime,startTime) % 1000 > 500 and state % 3 ==0:
                        nextTime = utime.ticks_ms()
                        #LED is completely off
                        t2ch1.pulse_width_percent(0)
                
            elif state % 3 ==1:
                print("Sine wave selected")
                #the button has been pressed and the state is 1
                while state % 3 == 1:
                    #as long as the time difference is greater than 0 and the state is 1,
                    #the sine wave will implement
                   while utime.ticks_diff(nextTime,startTime) > 0 and state % 3 ==1:
                        nextTime = utime.ticks_ms()
                        #sine function
                        sine_percent = 50*math.sin((math.pi/5)*x)+50
                        sine_brightness = t2ch1.pulse_width_percent(sine_percent)
                        #increments x by a small amount so that the LED pattern 
                        #is visible to the human eye
                        x += 0.005          
                        
            elif state % 3 ==2:            
                print("Saw wave selected")
                #the button has been pressed and the state is 2, as long as
                #the state is 2, the saw pattern can run
                while state % 3 == 2:
                    nextTime = utime.ticks_ms()
                    #the modulus operator resets the time difference after 1000 ms
                    #allowing the percent to follow a linear pattern
                    saw_percent = (utime.ticks_diff(nextTime,startTime) % 1000)/10
                    saw_brightness = t2ch1.pulse_width_percent(saw_percent)
                
            else:
                print("something is wrong!")
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break

    # Program de-initialization goes  here