## \addtogroup Lab0x01 Lab0x01
# @{

'''@file fibonacci.py
   @brief This computes the Fibonacci number for a given index
   @details The user is prompted to enter in a positive integer as an index to compute the fibonacci
   number for that user prompted index. This program uses an iterative process to avoid redundancies
   Link: https://bitbucket.org/jaint17/me305_labs/src/master/Lab1/fibonacci.py
   @author Tim Jain
   @date 01/20/21
'''

def fib (idx):
        '''@brief Here the fib function is defined which computes the Fibonacci number for a given index
           @details The function first defines initialization indices representing the 1st 2 indices
           of the Fibonacci sequence. It uses a for loop to recycle these values and then reassign new valus by setting
           the initialization values equal to each other and effectively adding those together. The for loop iterates from index 1 
           to the user specified index.
           @param idx This the integer specifying the index of the Fibonacci number 
           @return returns the value for the index if it is less than or equal to 1 
           and returns value of index that user inputs
        '''
        #idx_0 is the initialization index    
        #idx_1 is the 2nd index of Fibonacci
        idx_0 = 0        
        idx_1 = 1   
        #if the index is less than or equal to 1, the index is returned 
        if idx <= 1:  
            return idx
        #if the idx is greater than 1, then the fibonacci sequence begins e.g, fn = fn-1 + fn-2
        if idx > 1:  
        #for loop, the range is from 1 to the index the user specifies.
        #creates a new index corresponding to our two initial conditions above. This will change upon iterations
        #reassigns idx_0 to the value of idx_1
        #reassigns idx_1 to the value of idx_new
        #returns value to caller, exits function
            for n in range (1,idx):  
                idx_new = idx_0 + idx_1 
                idx_0 = idx_1  
                idx_1 = idx_new  
            return idx_1  
        #note that this process is not reusing old values upon each iteration, decreasing computation time. 
        #Since we are only concerned with the index the user specifies, it only concerns itself with that value.
        #Reassigning values for indices allows us to do this
        #if the index is smaller than 1, then the Fibonacci sequence will not run
        else:  
            print("Please choose a number greater than or equal to 0")
# This allows testing of your Fibonnaci function. Any code
# within the if __name__ == '__main__' block will only run when
# the script is executed as a standalone program. If the script
# is imported as a module the code block will not run.

# first the user's name is gathered and stored in a name variable
# then the user is prompted to enter in a index for the Fibonacci number.
# If the index is >=0, the fib function will be used to compute the Fibonacci number of the user specified index
# a dummy variable is created because the while loop will always run as long as it is equal to the inital value
# if the user inputs a value that is <0 or non numeric, the program will notify the user to try again

dummyvariable = 0
if __name__ == '__main__':
    name = input("Welcome to the Fibonacci sequence! You can press Ctr-C at any time to exit. First enter your name: ") 
    while dummyvariable == 0: 
        userinput = input("Greetings " + name + "! Now, please enter a positive integer. If you would like to compute the fibonacci sequence at the fifth index, simply enter 5 in the terminal!: ")
        if userinput.isnumeric():
            idx = int(userinput)
            if idx >= 0:   
                print ('The Fibonacci number at '' index {:} is {:}.'.format(idx,fib(idx)) + 'Choose another index or exit')
            else:
                print("Please try again and choose an integer greater than or equal to 0")
        else:
                print("Characters and Negative Numbers are not accepted. Please try again and choose an integer greater than or equal to 0")



